##Stage 1
FROM node:latest as node
COPY ./ /task-manager
WORKDIR /task-manager
RUN npm install
RUN npm install -g @angular/cli
RUN ng build --aot --prod

##Stage 2
FROM nginx:alpine
COPY --from=node /task-manager/dist/pragma-task/ /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
