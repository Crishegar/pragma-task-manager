import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {SnackBarService} from './core-components/snack-bar.service';
import {AuthService} from './home/auth.service';
import {HomeComponent} from './home/home/home.component';
import {LoginformComponent} from './home/loginform/loginform.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CoreComponentsModule} from './core-components/core-components.module';
import {HomeRoutingModule} from './home/home-routing.module';
import {ProjectsModule} from './projects/projects.module';
import {users} from './Mucks/user';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from '@angular/platform-browser';

describe('AppComponent', () => {
  let fixture;
  let app: AppComponent;
  let authService: AuthService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'home',
            component: HomeComponent
          }
        ]),
        HomeRoutingModule,
        CoreComponentsModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatIconModule,
        MatInputModule,
        ProjectsModule,
        MatTooltipModule,
        MatListModule,
        MatMenuModule,
        MatSnackBarModule,
        MatCardModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent,
        HomeComponent,
        LoginformComponent,
      ],
      providers: [SnackBarService, AuthService]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    authService = TestBed.get(AuthService);
    app = fixture.componentInstance;
  }));

  beforeEach(() => {
    const user = users[0];
    authService.login(user.username, user.password);
  });

  afterEach(() => {
    app.logout();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'My Task Manager'`, () => {
    expect(app.title).toEqual('My Task Manager');
  });

  it('Should be logout after click logout button', () => {
    expect(localStorage.getItem('task-user')).toEqual('cristian.garzon');
    fixture.detectChanges();
    const logoutButton = fixture.debugElement.query(By.css('#logout')).nativeElement;
    logoutButton.click();
    fixture.detectChanges();
    expect(localStorage.getItem('task-user')).not;
  });
});
