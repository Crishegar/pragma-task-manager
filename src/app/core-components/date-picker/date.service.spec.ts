import { TestBed } from '@angular/core/testing';

import {DateService, FORMAT_DATE} from './date.service';
import * as moment from 'moment';
import {Moment} from 'moment';

describe('DateService', () => {

  let service: DateService;
  let startDate;
  let millisecondsDate;
  let formatDate;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(DateService);
    startDate = moment().format('LLLL');
    millisecondsDate = Number(moment(startDate, 'LLLL').format('x'));
    formatDate = moment().format(FORMAT_DATE);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should be transform to milliseconds', () => {
    expect(service.transformDatePickerToMiliseconds(startDate)).toBe(millisecondsDate);
  });

  it('Should be fail to transform date to milliseconds', () => {
    expect(service.transformDatePickerToMiliseconds(startDate, FORMAT_DATE)).toBeFalsy();
  });

  it('Should be transform datePicker to date', () => {
    const date = moment().format('LL');
    const newDate = moment(date, 'LL');
    expect(service.transformFromDatePickerToFormatDate(newDate)).toBe(formatDate);
  });

  it('Should be no transform datePicker to date', () => {
    expect(service.transformFromDatePickerToFormatDate(formatDate)).toBe(formatDate);
  });

  it('Shoud be transform milliseconds to DatePicker', () => {
    expect(service.transformMilisecondsToDatePicker(millisecondsDate)).toBe(formatDate);
  });
});
