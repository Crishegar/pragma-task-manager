import {Injectable} from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateService {


  constructor() { }

  transformDatePickerToMiliseconds(value: string, format: string = 'LLLL') {
    return Number(moment(value, format).format('x'));
  }

  transformFromDatePickerToFormatDate(value: string | moment.Moment) {
    if (typeof value !== 'string')
      return moment(value, 'LL').format(FORMAT_DATE).toString();
    else
      return value;
  }

  transformMilisecondsToDatePicker(value: number) {
    if(typeof value === 'number')
      return moment.unix(value/1000).format(FORMAT_DATE);
  }
}
export const FORMAT_DATE = 'YYYY-MM-DD';
