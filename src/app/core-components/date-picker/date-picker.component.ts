import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {errors} from '../../Mucks/errors';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html'
})
export class DatePickerComponent implements OnInit, OnDestroy {
  @Input() name: string = 'Date';
  @Input() control: FormControl;
  subs: Subscription;
  error: string = 'required';

  constructor() {
  }

  ngOnInit() {
    this.subs = this.control.statusChanges.subscribe(status => {
      if (status == 'INVALID') {
        const errorsControl = Object.keys(this.control.errors);
        if (!!errors[errorsControl[0]]) this.error = errors[errorsControl[0]];
      }
    });
  }

  ngOnDestroy(): void {
    this.subs && this.subs.unsubscribe();
  }

}
