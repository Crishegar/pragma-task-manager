import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InputFormComponent} from './input-form.component';
import {MatIconModule, MatInputModule} from '@angular/material';
import {FormControl, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {errors} from '../../Mucks/errors';
import {By} from '@angular/platform-browser';

describe('InputFormComponent', () => {
  let component: InputFormComponent;
  let fixture: ComponentFixture<InputFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputFormComponent ],
      imports: [
        BrowserAnimationsModule,
        MatInputModule,
        MatIconModule,
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFormComponent);
    component = fixture.componentInstance;
    component.control = new FormControl('name', [Validators.compose([Validators.required])]);
    fixture.detectChanges();
  });

  afterEach(() => {
    component.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be show an error in a Email Validation', () => {
    component.control.setValidators([Validators.compose([Validators.email])]);
    component.control.setValue('ImNotAnEmail');
    fixture.detectChanges();
    expect(component.error).toEqual(errors.email);
  });

  it('Should be clear the data after click the clear button', () => {
    const value = 'Soy un valor'
    component.control.setValue(value);
    component.withClear = true;
    fixture.detectChanges();
    expect(component.control.value).toEqual(value);
    const clearButton = fixture.debugElement.query(By.css('button')).nativeElement;
    clearButton.click();
    fixture.detectChanges();
    expect(component.control.value).toBeNull();
  });
});
