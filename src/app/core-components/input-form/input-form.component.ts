import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {errors} from '../../Mucks/errors';

@Component({
  selector: 'input-form',
  templateUrl: './input-form.component.html',
})
export class InputFormComponent implements OnInit, OnDestroy {
  @Input() placeholder: string = '';
  @Input() control: FormControl;
  @Input() error: string = 'This field is required';
  @Input() type: string = 'text';
  @Input() withClear: boolean = false;
  @Output() clearEvent = new EventEmitter();
  subs: Subscription;

  constructor() { }

  ngOnInit() {
    this.subs = this.control.statusChanges.subscribe(status => {
      if (status == 'INVALID') {
        const errorsControl = Object.keys(this.control.errors);
        if (!!errors[errorsControl[0]])  this.error = errors[errorsControl[0]];
      }
    });
  }

  ngOnDestroy(): void {
    this.subs && this.subs.unsubscribe();
  }

  clear(event) {
    this.clearEvent.next(event);
    this.control.reset();
  }
}
