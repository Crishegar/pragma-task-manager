import {ComponentFixture, TestBed} from '@angular/core/testing';

import { SnackBarService } from './snack-bar.service';
import {MatSnackBarModule, MatSnackBarRef, SimpleSnackBar} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from '@angular/platform-browser';

describe('SnackBarService', () => {
  let service: SnackBarService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, BrowserAnimationsModule],
    });
    service = TestBed.get(SnackBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
