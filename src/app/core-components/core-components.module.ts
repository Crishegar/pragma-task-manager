import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardComponent} from './card/card.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule
} from '@angular/material';
import {InputFormComponent} from './input-form/input-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SnackBarService} from './snack-bar.service';
import {DatePickerComponent} from './date-picker/date-picker.component';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {SelectComponent} from './select/select.component';
import {ConfirmationDialogComponent} from './confirmation-dialog/confirmation-dialog.component';
import {DateService} from './date-picker/date.service';

@NgModule({
  declarations: [CardComponent, InputFormComponent, DatePickerComponent, SelectComponent, ConfirmationDialogComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatSelectModule
  ],
  exports: [
    CardComponent,
    InputFormComponent,
    DatePickerComponent,
    SelectComponent,
    ConfirmationDialogComponent
  ],
  providers: [
    SnackBarService,
    DateService,
  ]
})
export class CoreComponentsModule { }
