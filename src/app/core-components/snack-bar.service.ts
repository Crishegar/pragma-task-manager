import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action?: string) {
    (!!action) ? this.snackBar.open(message, action, {duration: 600}) : this.snackBar.open(message, '',  {duration: 600});
  }
}
