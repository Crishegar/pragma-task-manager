import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html'
})
export class SelectComponent implements OnInit {
  @Input() property: any;
  @Input() value: any;
  @Input() control: any;
  @Input() options: any;
  @Input() label: any;

  constructor() { }

  ngOnInit() {
  }

}
