import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {UserService} from '../home/user.service';

const routes: Routes = [
  {
    path: ':username',
    component: ProfileComponent,
    resolve: {
      user: UserService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
