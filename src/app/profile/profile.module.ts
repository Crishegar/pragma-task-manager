import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile/profile.component';
import {UserService} from '../home/user.service';
import {UserFormComponent} from '../home/user-form/user-form.component';
import {HomeModule} from '../home/home.module';
import {CoreComponentsModule} from '../core-components/core-components.module';
import {MatButtonModule} from '@angular/material';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    HomeModule,
    CoreComponentsModule,
    MatButtonModule
  ],
  providers: [UserService]
})
export class ProfileModule { }
