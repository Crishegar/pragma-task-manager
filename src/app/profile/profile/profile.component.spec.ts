import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileComponent} from './profile.component';
import {UserService} from '../../home/user.service';
import {MatSnackBarModule} from '@angular/material';
import {UserFormComponent} from '../../home/user-form/user-form.component';
import {CoreComponentsModule} from '../../core-components/core-components.module';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {IUser, users} from '../../Mucks/user';
import {User} from 'firebase';
import {By} from '@angular/platform-browser';
import {__assign} from 'tslib';
import {AuthService} from '../../home/auth.service';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let user: IUser;
  let userService: UserService;
  let authService : AuthService;
  let put;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent, UserFormComponent],
      imports: [
        MatSnackBarModule,
        CoreComponentsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        AuthService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: {
                user: of(users[1])
              }
            }
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = new UserService(authService);
    authService = TestBed.get(AuthService);
    TestBed.overrideProvider(UserService, {useValue: userService});
    user = __assign({}, users[0]);
    authService.login(user.username, user.password);
    put = spyOn(userService, 'put');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.user).toBeTruthy();
    expect(component.form).toBeTruthy();
  });

  it(`should be not update a user because invalid form`, () => {
    component.form.patchForm(user);
    fixture.detectChanges();
    component.updateUser();
    expect(put).not.toHaveBeenCalled();
  });
  //
  it(`should be not update a user because invalid name`, () => {
    user.username = users[1].username;
    component.form.form.patchValue(user);
    component.updateUser();
    fixture.detectChanges();
    expect(put).not.toHaveBeenCalled();
    expect(component.form.username.errors).toBeTruthy();
  });

  it(`should be update a user`, () => {
    user.username = 'newUsername';
    component.form.form.patchValue(user);
    component.updateUser();
    fixture.detectChanges();
    expect(userService.put).not.toHaveBeenCalled();
  });
});
