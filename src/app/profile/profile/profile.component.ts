import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IUser} from '../../Mucks/user';
import {UserFormComponent} from '../../home/user-form/user-form.component';
import {UserService} from '../../home/user.service';
import {SnackBarService} from '../../core-components/snack-bar.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit, AfterViewInit {

  user: IUser;
  @ViewChild(UserFormComponent) form: UserFormComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private cd: ChangeDetectorRef,
              private userService: UserService,
              private snack: SnackBarService) {
    this.user = this.activatedRoute.snapshot.data.user;
  }

  ngOnInit() {
    this.form.patchForm(this.user);
  }

  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  updateUser() {
    if (this.form.isValidForm$.value) {
      const value = this.form.form.value;
      value.password = (value && !!value.password) ? value.password.pass : this.user.password;
      value.projects = this.userService.getCurrentUser().projects;
      if (this.userService.existUserByUsername(value.username)) this.form.form.get('username').setErrors({usernameExist: true});
      const res = this.form.isValidForm$.value && this.userService.put(value);
      if (!!res) {
        this.form.patchForm(this.form.form.value);
        this.user = this.userService.getCurrentUser();
        this.snack.openSnackBar('Save data');
      }
    }
  }
}
