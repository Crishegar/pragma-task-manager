import {TestBed} from '@angular/core/testing';
import {TaskService} from './task.service';
import {UserService} from '../home/user.service';
import {DateService} from '../core-components/date-picker/date.service';
import {IUser, users} from '../Mucks/user';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBarModule} from '@angular/material';
import {ITask, tasks} from '../Mucks/task';
import {__assign} from 'tslib';
import {AuthService} from '../home/auth.service';
import * as moment from 'moment';

describe('TaskService', function () {

  let service: TaskService;
  let userService: UserService;
  let authService: AuthService;
  let dateService: DateService;
  let user;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [UserService, DateService]
    });
    service = TestBed.get(TaskService);
    userService = TestBed.get(UserService);
    authService = TestBed.get(AuthService);
    dateService = TestBed.get(DateService);
    user = users[1];
    authService.user = user;
  });

  it('Should be get tasks by user', () => {
    expect(service.getTasksByUser(user)).toEqual(tasks);
  });

  it('Should be no get tasks by user', () => {
    const otherUser: IUser = __assign({}, user);
    otherUser.tasks = [];
    expect(service.getTasksByUser(otherUser)).toEqual([]);
  });

  it('Should be not exist Task by name because is the sameTask', () => {
    const task = tasks[0];
    expect(service.existTaskByName(task)).toBeFalsy();
  });

  it('should be Exist task by name', () => {
    const task = __assign({}, tasks[0]);
    task.id = null;
    expect(service.existTaskByName(task)).toBeTruthy();
  });

  it('Should be not exist task by name', () => {
    const task = __assign({}, tasks[0]);
    task.name = 'im a great task';
    task.id = null;
    expect(service.existTaskByName(task)).toBeFalsy();
  });

  it('Should be delete a task for user', () => {
    expect(service.removeTask(user, '5')).toBeTruthy();
  });

  it('Should be not delete a task for user Because do not exist the task', () => {
    expect(service.removeTask(user, '55')).toBeFalsy();
  });

  it('Should be create a task', () => {
    const newTask: ITask = {
      name: 'new Task',
      limitDate: dateService.transformDatePickerToMiliseconds(moment().format('LLLL')),
      complete: false,
      comments: [],
      project: {id: '1'},
      id: null
    };
    expect(service.post(newTask)).toBeTruthy();
  });

  it('Should be complete task', () => {
    const newTask: ITask = {
      name: 'new Task',
      limitDate: dateService.transformDatePickerToMiliseconds(moment().format('LLLL')),
      complete: false,
      comments: [],
      project: {id: '1'},
      id: '99'
    };
    service.post(newTask);
    expect(service.completeTask(user, '99')).toBeTruthy();
    expect(newTask.complete).toBeTruthy();
  });

  it('Should be get full task', () => {
    expect(service.isFull(user.tasks)).toBeTruthy();
  });

  it('Should be no get full task', () => {
    const otherUser = __assign({}, user);
    otherUser.tasks.pop();
    expect(service.isFull(otherUser)).toBeFalsy();
  });

  it('Should be update a task', () => {
    const newTask: ITask = {
      name: 'new Task',
      limitDate: dateService.transformDatePickerToMiliseconds(moment().format('LLLL')),
      complete: false,
      comments: [],
      project: {id: '1'},
      id: '1'
    };
    expect(service.put(newTask)).toBeTruthy();
  });

  it('Should be no update a task', () => {
    const newTask: ITask = {
      name: 'new Task',
      limitDate: dateService.transformDatePickerToMiliseconds(moment().format('LLLL')),
      complete: false,
      comments: [],
      project: {id: '1'},
      id: '88'
    };
    expect(service.put(newTask)).toBeFalsy();
  });
});
