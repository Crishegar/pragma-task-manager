import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SnackBarService} from '../../core-components/snack-bar.service';
import {IProject, ITask} from '../../Mucks/task';
import {TaskService} from '../task.service';
import {ProjectService} from '../../projects/project.service';
import {TaskFormComponent} from '../task-form/task-form.component';
import {DateService} from '../../core-components/date-picker/date.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html'
})
export class TaskComponent implements OnInit, AfterViewInit {

  task: ITask = null;
  project: IProject;
  @ViewChild(TaskFormComponent) form: TaskFormComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private snack: SnackBarService,
              private taskService: TaskService,
              private projectService: ProjectService,
              private cd: ChangeDetectorRef,
              private dateService: DateService) {
    this.task = this.activatedRoute.snapshot.data.task;
    this.project = this.projectService.getProjectById(this.task.project.id);
  }

  ngAfterViewInit(): void {
    this.form.patchValue(this.task);
    this.cd.detectChanges();
  }

  ngOnInit() {
  }

  saveTask() {
    const value = this.form.form.value;
    value.limitDate = this.dateService.transformDatePickerToMiliseconds(this.form.limitDate.value);
    if(this.taskService.existTaskByName(value)) this.form.name.setErrors({existName: true});
    if(this.form.isValid$.value){
      this.taskService.put(value);
    }
  }
}
