import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProjectsRoutingModule} from './projects-routing.module';
import {ProjectsComponent} from './projects/projects.component';
import {MatButtonModule, MatIconModule, MatListModule} from '@angular/material';
import {TaskComponent} from './task/task.component';
import {TaskFormComponent} from './task-form/task-form.component';
import {CoreComponentsModule} from '../core-components/core-components.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [ProjectsComponent, TaskComponent, TaskFormComponent],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    MatIconModule,
    MatButtonModule,
    CoreComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule
  ],
  exports: [TaskFormComponent]
})
export class ProjectsModule {
}
