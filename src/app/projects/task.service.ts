import {Injectable} from '@angular/core';
import {ITask, tasks} from '../Mucks/task';
import {IUser} from '../Mucks/user';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../home/user.service';
import {DateService} from '../core-components/date-picker/date.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService implements Resolve<ITask> {

  constructor(private userService: UserService, private dateService: DateService) {
  }

  getTasksByUser(user: IUser) {
    return tasks.reduce((pv, cv) => {
      user.tasks.forEach(task => {
        if (task.id === cv.id) {
          if (typeof cv.limitDate === 'number')
            cv.limitDate = this.dateService.transformMilisecondsToDatePicker(cv.limitDate);
          pv.push(cv);
        }
      });
      return pv;
    }, []);
  }

  existTaskByName(task: ITask) {
    const existTask = tasks.find(val => {
        if (val.name.toLocaleLowerCase() === task.name.toLocaleLowerCase()) {
          if (!!task.id) {
            return (val.id !== task.id);
          } else
            return true;
        }
        return false;
      }
    );
    return !!existTask;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITask> | Promise<ITask> | ITask | null {
    const task = this.getTaskById(route.params.id);
    if (task) return Promise.resolve(task);
    return Promise.reject(null);
  }

  private getTaskById(id: string) {
    return tasks.find(task => task.id === id);
  }

  isFull(any: ITask[]): boolean {
    return JSON.stringify(any) === JSON.stringify(this.userService.getCurrentUser().tasks);
  }

  put(value: any) {
    const index = tasks.findIndex(val => val.id === value.id);
    if (index >= 0) tasks[index] = value;
    return (index >= 0);
  }

  post(task: ITask) {
    const user = this.userService.getCurrentUser();
    task = this.putId(task);
    tasks.push(task);
    user.tasks.push(task);
    const index = tasks.findIndex(val => val.id === task.id);
    const indexTaskUser = user.tasks.findIndex(val => val.id === task.id);
    return (index >= 0 && indexTaskUser >= 0);
  }

  private putId(task: ITask) {
    if (!task.id) {
      task.id = (tasks.length + 1).toString();
    }
    return task;
  }

  removeTask(currentUser: IUser, id: string) {
    const taskIndex = this.getTasksByUser(currentUser).findIndex(task => task.id === id);
    if (taskIndex >= 0) this.userService.getCurrentUser().tasks.splice(taskIndex, 1);
    return taskIndex >= 0;
  }

  completeTask(currentUser: IUser, id: string) {
    const task: ITask = this.getTasksByUser(currentUser).find(task => task.id === id);
    task.complete = true;
    return (!!task);
  }
}
