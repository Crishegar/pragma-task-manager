import { Injectable } from '@angular/core';
import {projects} from '../Mucks/task';
import {IUser} from '../Mucks/user';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor() { }

  getProjects(user: IUser) {
    return projects.reduce((pv, cv) => {
      user.projects.map(element => {
        if(element.id === cv.id){
          pv.push(cv);
        }
      });
      return pv;
    }, []);
  }

  getProjectById(id: string){
    return projects.find(project => project.id === id)
  }
}
