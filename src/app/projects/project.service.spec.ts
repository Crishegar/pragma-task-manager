import { TestBed } from '@angular/core/testing';

import { ProjectService } from './project.service';
import {users} from '../Mucks/user';
import {projects} from '../Mucks/task';

describe('ProjectService', () => {

  let service: ProjectService;
  let user;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ProjectService);
    user = users[1];
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should be get Projects', () => {
    expect(service.getProjects(user)).toEqual(projects);
  });

  it('should be get project by id', () => {
    expect(service.getProjectById('1')).toBe(projects[0]);
  });

  it('Should be not get project by id', () => {
    expect(service.getProjectById('55')).toBeUndefined();
  })
});
