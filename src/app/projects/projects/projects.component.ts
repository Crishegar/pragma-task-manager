import {Component, OnInit} from '@angular/core';
import {IUser} from '../../Mucks/user';
import {UserService} from '../../home/user.service';
import {IProject, ITask} from '../../Mucks/task';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '../project.service';
import {TaskService} from '../task.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
})
export class ProjectsComponent implements OnInit {

  projects: IProject[] = [];
  user: IUser;
  tasks: ITask[] = [];

  constructor(private userService: UserService,
              private taskService: TaskService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private projectService: ProjectService) {
    this.user = this.userService.getCurrentUser();
    this.projects = this.projectService.getProjects(this.user);
    this.tasks = this.taskService.getTasksByUser(this.user)
      .sort((a, b) => a.project.id - b.project.id);
  }

  ngOnInit() {
  }

  goToDetails(id: number) {
    this.router.navigate(['./task', id], {relativeTo: this.activatedRoute})
  }
}
