import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectsComponent} from './projects/projects.component';
import {TaskComponent} from './task/task.component';
import {TaskService} from './task.service';

const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent
  },
  {
    path: 'task/:id',
    component: TaskComponent,
    resolve: {
      task: TaskService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
