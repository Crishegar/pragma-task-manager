import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IProject, ITask} from '../../Mucks/task';
import {DateService} from '../../core-components/date-picker/date.service';
import {UserService} from '../../home/user.service';
import {BehaviorSubject} from 'rxjs';
import {ProjectService} from '../project.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html'
})
export class TaskFormComponent implements OnInit, AfterViewInit {

  @Input() isUpdate: boolean = false;
  form: FormGroup;
  projects: IProject[] = [];
  isValid$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  initialValue: any;

  constructor(private fb: FormBuilder,
              private dateService: DateService,
              private projectService: ProjectService,
              private userService: UserService) { }

  ngOnInit() {
    this.initForm();
    this.putProjects();
  }

  private initForm() {
    this.form = this.fb.group({
      id: [''],
      name: ['', Validators.compose([Validators.required])],
      limitDate: ['', Validators.compose([Validators.required])],
      project: this.fb.group({
        id: ['', Validators.compose([Validators.required])]
      }),
      comments: this.fb.array([])
    });
  }

  addComment() {
    const control = new FormControl('', Validators.compose([Validators.max(200), Validators.min(20)]));
    this.comments.push(control);
  }

  get comments(): FormArray {
    return this.form.get('comments') as FormArray;
  }

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get limitDate(): FormControl {
    return this.form.get('limitDate') as FormControl;
  }

  get projectId(): FormControl {
    return this.form.get('project.id') as FormControl;
  }

  patchValue(value: ITask){
    const form = value;
    if(Number(form.limitDate))
      form.limitDate = this.dateService.transformMilisecondsToDatePicker(value.limitDate).toString();
    if (form.comments.length >= 0) form.comments.forEach(() => this.addComment());
    this.form.patchValue(form);
    this.initialValue = this.form.value;
    this.isValid$.next(false);
  }

  private putProjects() {
    this.projects = this.projectService.getProjects( this.userService.getCurrentUser() );
  }

  ngAfterViewInit(): void {
        this.form.statusChanges.subscribe(val => {
          if(!!this.initialValue){
        const form = this.form.value;
        form.limitDate = this.dateService.transformFromDatePickerToFormatDate(form.limitDate);
        this.isValid$.next((JSON.stringify(this.initialValue) !== JSON.stringify(form) && this.form.valid));
      } else {
        this.isValid$.next(!(val === 'INVALID'));
      }
    })
  }

  removeComment(i: number) {
    this.comments.removeAt(i);
  }
}
