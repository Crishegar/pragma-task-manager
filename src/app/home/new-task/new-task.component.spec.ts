import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewTaskComponent} from './new-task.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef, MatIconModule, MatSnackBarModule} from '@angular/material';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {TaskFormComponent} from '../../projects/task-form/task-form.component';
import {CoreComponentsModule} from '../../core-components/core-components.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ProjectService} from '../../projects/project.service';
import {AuthService} from '../auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import * as moment from 'moment';
import {tasks} from '../../Mucks/task';
import {TaskService} from '../../projects/task.service';
import {Test} from 'tslint';
import {__assign} from 'tslib';

describe('NewTaskComponent', () => {
  let component: NewTaskComponent;
  let fixture: ComponentFixture<NewTaskComponent>;
  let authService: AuthService;
  let newTask;
  let taskService: TaskService;

  const mockDialog = {
    close: jasmine.createSpy('close')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewTaskComponent, TaskFormComponent],
      imports: [
        MatDialogModule,
        CoreComponentsModule,
        ReactiveFormsModule,
        MatIconModule,
        MatSnackBarModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        {provide: MatDialogRef, useValue: mockDialog},
        {provide: MAT_DIALOG_DATA, useValue: FormGroup},
        AuthService,
        TaskService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTaskComponent);
    component = fixture.componentInstance;
    taskService = TestBed.get(TaskService);
    authService = TestBed.get(AuthService);
    authService.login('admin', 'admin');
    fixture.detectChanges();
  });

  beforeEach(() => {
    newTask = {
      id: '',
      name: '',
      limitDate: moment().format('LLLL'),
      project: {
        id: '1'
      },
      comments: [
        'asdfasdfasdfasdf',
        'asdfasdfasdf'
      ]
    }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should be fail to create a task`, () => {
    newTask.name = tasks[0].name;
    component.form.form.patchValue(newTask);
    fixture.detectChanges();
    component.saveTask();
    expect(component.form.form.get('name').errors).toBeTruthy();
  });

  it('should be create a task', () => {
    component.form.form.patchValue(newTask);
    component.form.name.setValue('newTask');
    component.form.isValid$.next(true);
    const taskCreate = __assign({}, component.form.form.value);
    component.saveTask();
    expect(taskService.existTaskByName(taskCreate)).toBeTruthy();
  });

  it(`should be remove a comment`, () => {
    newTask.comments.push('   ');
    expect(newTask.comments.length).toEqual(3);
    component.form.patchValue(newTask);
    component.saveTask();
    newTask = component.form.form.value;
    expect(newTask.comments.length).toEqual(2);
  });
});
