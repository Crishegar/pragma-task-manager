import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormGroup} from '@angular/forms';
import {SnackBarService} from '../../core-components/snack-bar.service';
import {DateService} from '../../core-components/date-picker/date.service';
import {TaskFormComponent} from '../../projects/task-form/task-form.component';
import {TaskService} from '../../projects/task.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html'
})
export class NewTaskComponent {

  @ViewChild(TaskFormComponent) form: TaskFormComponent;

  constructor(public dialogRef: MatDialogRef<NewTaskComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FormGroup,
              private snack: SnackBarService,
              private dateService: DateService,
              private taskService: TaskService) { }
  closeDialog() {
    this.dialogRef.close();
  }

  saveTask() {
    const task = this.form.form.value;
    if (this.taskService.existTaskByName(task)) this.form.name.setErrors({existName: true});
    task.comments = this.cleanComments(task.comments);
    task.limitDate = this.dateService.transformDatePickerToMiliseconds(task.limitDate);
    const save = (this.form.isValid$.value) ? this.taskService.post(task) : false;
    if (!!save) {
      this.snack.openSnackBar('Save successful', 'Save');
      this.closeDialog();
    }
  }

  private cleanComments(comments: string[]) {
    return comments.reduce((pv, cv) => {
      if (cv.trim() && cv !== '') {
        pv.push(cv);
      }
      return pv;
    }, [])
  }
}
