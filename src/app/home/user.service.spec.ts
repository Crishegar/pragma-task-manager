import {inject, TestBed} from '@angular/core/testing';

import {UserService} from './user.service';
import {AuthService} from './auth.service';
import {SnackBarService} from '../core-components/snack-bar.service';
import {MatSnackBarModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {IUser, users} from '../Mucks/user';
import {__assign} from 'tslib';

describe('UserService', () => {

  let authService: AuthService;
  let snackBarService: SnackBarService;
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule
      ],
      declarations: [],
      providers: [AuthService, SnackBarService]
    });
    service = TestBed.get(UserService);
    authService = TestBed.get(AuthService);
    snackBarService = TestBed.get(SnackBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be exist an AuthService', () => {
    inject([AuthService], (injectAuthService: AuthService) => {
      expect(injectAuthService).toBe(authService);
    });
  });

  it('should be find an User by Username', () => {
    const username = 'admin';
    expect(service.existUserByUsername(username)).toBeTruthy();
  });

  it('should not be find an User by Username', () => {
    const username = 'otherUser';
    expect(service.existUserByUsername(username)).toBeFalsy();
  });

  it('should be find an User by Email', () => {
    const email = 'cristian.garzon@pragma.com.co';
    expect(service.existUserByEmail(email)).toBeTruthy();
  });

  it('should not be find an User by Email', () => {
    const email = 'asdfasd@adfasdf.com';
    expect(service.existUserByEmail(email)).toBeFalsy();
  });

  it('should be update an User data', () => {
    const user: IUser = users[1];
    authService.login(user.username, user.password);
    const otherUser = user;
    otherUser.name = 'Soy un nuevo User';
    expect(service.put(otherUser)).toBeTruthy();
  });

  it('should not be update an User data', () => {
    const user: IUser = users[1];
    authService.login(user.username, user.password);
    const otherUser = __assign({}, user);
    otherUser.username = '159951';
    expect(service.put(otherUser)).toBeTruthy();
  });

  it('should not be update an User By do not exist', () => {
    const user: IUser = users[1];
    authService.login(user.username, user.password);
    const otherUser = __assign({}, user);
    authService.user = otherUser;
    otherUser.username = 'SoayDiferente';
    expect(service.put(otherUser)).toBeFalsy();
  });

  it('register a new User', () => {
    const user: IUser = {
      name: 'newUser',
      lastName: 'new User',
      username: 'newUser',
      password: 'user',
      email: 'user@user.com',
      tasks: [],
      projects: []
    };
    expect(service.signUp(user)).toBeTruthy();
  });
});
