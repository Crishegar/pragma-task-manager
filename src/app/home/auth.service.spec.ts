import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {SnackBarService} from '../core-components/snack-bar.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBarModule} from '@angular/material';
import {users} from '../Mucks/user';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('AuthService', () => {

  let service: AuthService;
  let snackBar: SnackBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSnackBarModule,
        BrowserAnimationsModule
      ],
      providers: [SnackBarService],
    });
    service = TestBed.get(AuthService);
    snackBar = TestBed.get(SnackBarService);
    if(localStorage.getItem('task-user')){
      localStorage.removeItem('task-user');
    }
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should be fail login', () => {
    const username = 'admin';
    const pass = 'password';
    expect(service.login(username, pass)).toBeFalsy();
  });

  it('Should be fail to find a login user', () => {
    expect(service.existLogUser('admin')).toBeFalsy();
  });

  it('Should be find a login user', () => {
    const user = users[0];
    service.login(user.username, user.password);
    expect(service.existLogUser(user.username)).toBeUndefined();
  });

  it('Should be logout', () => {
    const username = 'admin';
    const pass = 'admin';
    service.login(username, pass);
    expect(localStorage.getItem('task-user')).toBe('Admin');
    service.logout();
    expect(localStorage.getItem('task-user')).toEqual(null);
  });

  it('Should be fail logout', () => {
    expect(service.logout()).toBeFalsy();
  })
});
