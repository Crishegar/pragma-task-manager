import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {SignupFormComponent} from '../signup-form/signup-form.component';
import {MatDialog} from '@angular/material';
import {UserService} from '../user.service';
import {ITask} from '../../Mucks/task';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {NewTaskComponent} from '../new-task/new-task.component';
import {TaskService} from '../../projects/task.service';
import {ConfirmationDialogComponent} from '../../core-components/confirmation-dialog/confirmation-dialog.component';
import {SnackBarService} from '../../core-components/snack-bar.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {

  tasks: ITask[] = [];
  searchForm: FormGroup;
  subs: Subscription;
  oldValue: string = '';
  isPhone: boolean = false;

  constructor(public auth: AuthService,
              private dialog: MatDialog,
              private userService: UserService,
              private taskService: TaskService,
              private fb: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snack: SnackBarService) {
  }

  ngOnInit() {
    this.putTasks();
    this.initForm();
    this.isPhone = window.innerWidth <= 840;
  }

  openDialog() {
    this.dialog.open(SignupFormComponent, {width: '400px', data: {}});
  }

  private initForm() {
    this.searchForm = this.fb.group({
      search: ['']
    });
  }

  get search(): FormControl {
    return this.searchForm.get('search') as FormControl;
  }

  private putTasks() {
    this.auth.isLogged$.subscribe(val => {
      if (val)
        this.tasks = this.taskService.getTasksByUser(this.userService.getCurrentUser());
      else
        this.tasks = [];
    });
  }

  findTask() {
    if(!this.search.pristine) {
      if (!!this.search.value) {
        if (this.search.value != this.oldValue) {
          this.putTasks();
          this.tasks = this.tasks.filter(val => val.name.toLocaleLowerCase()
            .includes(this.search.value.toString().toLocaleLowerCase()));
          this.oldValue = this.search.value;
        }
      } else {
        if ( !this.taskService.isFull(this.tasks)) {
          this.putTasks();
        }
      }
    }
  }

  cleanSearch() {
    this.putTasks();
  }

  ngOnDestroy(): void {
    this.subs && this.subs.unsubscribe();
  }

  addTask() {
    this.dialog.open(NewTaskComponent, {width: '400px', data: {}});
    this.dialog.afterAllClosed.subscribe(() => this.putTasks());
  }

  goToDetails(id: number) {
    this.router.navigate(['../projects/task', id], {relativeTo: this.activatedRoute})
  }

  removeTask(id: number) {
    let deleted: boolean, message: string = 'fail to deleted', action: string = 'Fail';
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {message: 'Are you sure that Delete this task?', title: 'Delete task'}
    });
    dialogRef.componentInstance.confirmation = () => {
      deleted = this.taskService.removeTask(this.userService.getCurrentUser(), id.toString());
      if (deleted) {
        message = 'deleted successful';
        action = 'Deleted';
        this.putTasks();
      }
      this.snack.openSnackBar(message, action);
      dialogRef.close();
    };
  }

  completeTask(id: number) {
    let completed: boolean, message: string = 'fail to complete task';
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {message: 'Are you sure that complete this task?', title: 'Completed task'}
    });
    dialogRef.componentInstance.confirmation = () => {
      completed = this.taskService.completeTask(this.userService.getCurrentUser(), id.toString());
      dialogRef.close();
      if (completed) {
        message = 'The task is completed';
      }
      this.snack.openSnackBar(message);
      dialogRef.close();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event){
    this.isPhone = (event.target.innerWidth <= 840);
  }
}
