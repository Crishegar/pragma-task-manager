import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IUser, users} from '../Mucks/user';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService implements Resolve<IUser> {

  constructor(private auth: AuthService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUser> | Promise<IUser> | IUser {
    const username = route.params.username;
    const user = this.existUserByUsername(username);
    return (!!user) ? Promise.resolve(user) : Promise.reject(null);
  }

  put(value: IUser) {
    const index = users.findIndex(user => user.username.toLocaleLowerCase() === this.getCurrentUser().username.toLocaleLowerCase());
    if (index >= 0) {
      users[index] = value;
      if (users[index].username.toLocaleLowerCase() !== this.getCurrentUser().username.toLocaleLowerCase()) {
        localStorage.setItem('task-user', users[index].username);
      }
      return true;
    }
    return false;
  }

  existUserByUsername(username: string): IUser | null {
    const existUser = users.find(val => username.toLocaleLowerCase() === val.username.toLocaleLowerCase());
    return (!!existUser) ? existUser : null;
  }

  existUserByEmail(email: string): IUser | null {
    const existUser = users.find(val => email.toLocaleLowerCase() === val.email.toLocaleLowerCase());
    return (!!existUser) ? existUser : null;
  }

  signUp(value: any) {
    return this.auth.signUp(value);
  }

  getCurrentUser(): IUser | null {
    return this.auth.getCurrentUser();
  }
}
