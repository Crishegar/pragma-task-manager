import {AfterViewInit, Component, Input, OnDestroy} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, Subscription} from 'rxjs';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',

})
export class UserFormComponent implements AfterViewInit, OnDestroy {

  public form: FormGroup = new FormGroup({});
  @Input() isUpdate: boolean = false;
  isUpdatePass$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  subs: Subscription[] = [];
  initialValue: any;
  isValidForm$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private fb: FormBuilder) {
    this.initForm();
  }

  get username(): FormControl {
    return this.form.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.form.get('password').get('pass') as FormControl;
  }

  get confirmationPass(): FormControl {
    return this.form.get('password').get('confirmationPass') as FormControl;
  }

  get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get lastName(): FormControl {
    return this.form.get('lastName') as FormControl;
  }

  get email(): FormControl {
    return this.form.get('email') as FormControl;
  }

  private initForm() {
    this.form = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: this.fb.group({
        pass: ['', Validators.compose([Validators.required])],
        confirmationPass: ['', Validators.compose([Validators.required])],
      }, {validator: confirmationPass})
    });
  }

  public patchForm(value) {
    this.form.patchValue(value);
    this.initialValue = this.form.value;
    this.isValidForm$.next(false);
    this.isUpdatePass$.next(false);
  }

  showPassword() {
    this.isUpdatePass$.next(!this.isUpdatePass$.value);
  }

  ngAfterViewInit(): void {
    this.subsToUpdatePass();
    this.subsToStatusChange();
  }

  ngOnDestroy(): void {
    this.subs.forEach(subs => subs.unsubscribe());
  }

  private subsToUpdatePass() {
    this.subs.push(this.isUpdatePass$.subscribe(val => {
      if (!val) {
        this.form.removeControl('password');
      } else {
        this.form.addControl('password', this.fb.group({
          pass: ['', Validators.compose([Validators.required])],
          confirmationPass: ['', Validators.compose([Validators.required])],
        }, {validator: confirmationPass}));
      }
    }));
  }

  private subsToStatusChange() {
    this.subs.push(this.form.statusChanges.subscribe(val => {
      if (!!this.initialValue) {
        if (!this.isUpdatePass$.value) delete this.initialValue.password;
        this.isValidForm$.next((JSON.stringify(this.initialValue) !== JSON.stringify(this.form.value) && this.form.valid));
      } else {
        this.isValidForm$.next(!(val === 'INVALID'));
      }
    }));
  }
}

export const confirmationPass = (control: AbstractControl): { [key: string]: boolean } | null => {
  const pass = control.get('pass');
  const confirmation = control.get('confirmationPass');
  if (!pass.value || !confirmation.value) return null;
  if (pass.value === confirmation.value) {
    return null;
  } else {
    confirmation.setErrors({invalidPass: true});
    return {invalidPass: true};
  }
};
