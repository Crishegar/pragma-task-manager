import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {SnackBarService} from '../../core-components/snack-bar.service';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
})
export class LoginformComponent implements OnInit {

  form: FormGroup;
  holi: string = 'This field is required';

  constructor(private fb: FormBuilder, private auth: AuthService,
              private snack: SnackBarService) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.form = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
}

  get username(): FormControl {
    return this.form.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.form.get('password') as FormControl;
  }

  login() {
    const existUser = this.auth.login(this.username.value, this.password.value);
    (!existUser) ? this.username.setErrors({invalidUser: true}) : this.snack.openSnackBar('login successful');
  }
}
