import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginformComponent} from './loginform.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CoreComponentsModule} from '../../core-components/core-components.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {SnackBarService} from '../../core-components/snack-bar.service';
import {MatSnackBarModule} from '@angular/material';
import {AuthService} from '../auth.service';

describe('LoginformComponent', () => {
  let component: LoginformComponent;
  let fixture: ComponentFixture<LoginformComponent>;
  let authService: AuthService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginformComponent],
      imports: [
        ReactiveFormsModule,
        CoreComponentsModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [SnackBarService, AuthService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginformComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be fail because wrong user or password', () => {
    const username = 'ImAUser';
    const pass = '159375';
    component.username.setValue(username);
    component.password.setValue(pass);
    expect(component.login()).toBeFalsy();
    expect(component.username.errors).toEqual({invalidUser: true});
  });

  it('should be login', () => {
    const username = 'admin';
    const pass = 'admin';
    component.username.setValue(username);
    component.password.setValue(pass);
    component.login();
    expect(localStorage.getItem('task-user')).toEqual('Admin');
  })
});
