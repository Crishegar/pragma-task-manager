import {Component, Inject, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UserFormComponent} from '../user-form/user-form.component';
import {SnackBarService} from '../../core-components/snack-bar.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
})
export class SignupFormComponent {

  @ViewChild(UserFormComponent) form: UserFormComponent;

  constructor(private user: UserService,
              private snack: SnackBarService,
              public dialogRef: MatDialogRef<SignupFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: FormGroup) {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  signUp() {
    const user = this.form.form.value;
    user.password = this.form.form.value.password.pass;
    if (this.user.existUserByUsername(this.form.username.value) != null) this.form.username.setErrors({exist: true});
    if (this.user.existUserByEmail(this.form.email.value) != null) this.form.email.setErrors({exist: true});
    const register  = (this.form.form.valid) ? this.user.signUp(this.form.form.value) : null;
    if (!!register) {
      this.snack.openSnackBar('Register successful');
      this.form.form.reset();
      this.closeDialog();
    }
  }
}
