import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SignupFormComponent} from './signup-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from '../user.service';
import {UserFormComponent} from '../user-form/user-form.component';
import {CoreComponentsModule} from '../../core-components/core-components.module';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef, MatSnackBarModule} from '@angular/material';

describe('SignupFormComponent', () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;
  let userService: UserService;
  const mockDialog = {
    close: jasmine.createSpy('close')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupFormComponent, UserFormComponent ],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        CoreComponentsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule
      ],
      providers: [UserService, {provide: MatDialogRef, useValue: mockDialog }, {provide: MAT_DIALOG_DATA, useValue: FormGroup}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be form is empty', () => {
    const value = {username: '', name: '', lastName: '', email: '', password :{ pass: '', confirmationPass: ''}};
    expect(component.form.form.value).toEqual(value);
  });

  it(`should be don't register a new user`, () => {
    const value = {username: 'admin', name: 'name', lastName: 'last', email: 'a', password :{ pass: 'a', confirmationPass: 'b'}};
    component.form.form.setValue(value);
    component.signUp();
    fixture.detectChanges();
    expect(localStorage.getItem('task-user')).not;
    expect(userService.existUserByUsername(value.username)).toBeTruthy();
    expect(userService.existUserByEmail(value.email)).toBeNull();
  });

  it(`should be don't register a new user`, () => {
    const value = {username: 'newUser', name: 'name', lastName: 'last', email: 'cristian.garzon1495@gmail.com', password :{ pass: 'a', confirmationPass: 'a'}};
    component.form.form.setValue(value);
    component.signUp();
    fixture.detectChanges();
    expect(localStorage.getItem('task-user')).not;
    expect(userService.existUserByUsername(value.username)).toBeNull();
    expect(userService.existUserByEmail(value.email)).toBeTruthy();
  });

  it('should be register a new user', () => {
    const value = {username: 'newUser', name: 'name', lastName: 'last', email: 'newUser@user', password :{ pass: 'a', confirmationPass: 'a'}};
    component.form.form.setValue(value);
    component.signUp();
    fixture.detectChanges();
    expect(userService.existUserByUsername(value.username)).toBeTruthy();
    expect(userService.existUserByEmail(value.email)).toBeTruthy();
    expect(localStorage.getItem('task-user')).toEqual(value.username);
  });

});
