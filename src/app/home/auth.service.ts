import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {IUser, users} from '../Mucks/user';
import {SnackBarService} from '../core-components/snack-bar.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLogged$ = new BehaviorSubject(false);
  user: IUser = null;

  constructor(private router: Router, private snack: SnackBarService) {
  }

  login(username: string, password: string): boolean {
    const user = users.find(val => val.username.toLocaleLowerCase() === username.toLocaleLowerCase());
    if (!!user && (user.password === password)) {
      localStorage.setItem('task-user', user.username);
      this.user = user;
      this.isLogged$.next(true);
      return true;
    }
    return false;
  }

  logout(): boolean {
    if (!!this.isLogged$.getValue()) {
      this.user = null;
      localStorage.removeItem('task-user');
      this.isLogged$.next(false);
      this.router.navigate(['../home']);
      this.snack.openSnackBar('logout successful');
      return true;
    }
  }

  signUp(user: IUser): boolean {
    users.push(user);
    this.login(user.username, user.password);
    return true;
  }

  existLogUser(username: string) {
    const index = users.findIndex(val => username === val.username);
    if (index > -1) {
      this.user = users[index];
      this.isLogged$.next(true);
    } else {
      localStorage.removeItem('task-user');
    }
  }

  getProfileRoute(): string {
    return `/profile/${localStorage.getItem('task-user')}`;
  }

  getCurrentUser() {
    return this.user;
  }
}
