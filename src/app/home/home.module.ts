import {NgModule} from '@angular/core';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home/home.component';
import {CoreComponentsModule} from '../core-components/core-components.module';
import {MatButtonModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatTooltipModule} from '@angular/material';
import {LoginformComponent} from './loginform/loginform.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SignupFormComponent} from './signup-form/signup-form.component';
import {UserFormComponent} from './user-form/user-form.component';
import {SnackBarService} from '../core-components/snack-bar.service';
import { NewTaskComponent } from './new-task/new-task.component';
import {ProjectsModule} from '../projects/projects.module';
import {ConfirmationDialogComponent} from '../core-components/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [HomeComponent, LoginformComponent, SignupFormComponent, UserFormComponent, NewTaskComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CoreComponentsModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    ProjectsModule,
    MatTooltipModule,
    MatListModule,
    MatMenuModule
  ],
  entryComponents: [SignupFormComponent, NewTaskComponent, ConfirmationDialogComponent],
  exports: [UserFormComponent],
  providers: [SnackBarService]
})
export class HomeModule { }
