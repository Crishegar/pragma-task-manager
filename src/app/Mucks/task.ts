export interface IProject {
  id: string;
  name: string;
}

export interface ITask {
  id: string;
  name: string;
  project: {id: string};
  limitDate: any;
  comments: string[];
  complete: boolean;
}

export const projects: IProject[] = [
  {
    id: '1',
    name: 'Bancolombia'
  },
  {
    id: '2',
    name: 'Confama'
  },
  {
    id: '3',
    name: 'EPM'
  }
];

export const tasks: ITask[] = [
  {
    id: '1',
    name: 'Primer Tarea Confama',
    limitDate: 1575694800000,
    complete: false,
    project : {
      id: '2'
    },
    comments: ['Hola soy un comentario']
  },
  {
    id: '2',
    name: 'Segunda Tarea Confama',
    limitDate: 1575694800000,
    complete: false,
    project : {
      id: '2'
    },
    comments: []
  },
  {
    id: '3',
    name: 'Tercer Tarea Confama',
    limitDate: 1575694800000,
    complete: true,
    project : {
      id: '2'
    },
    comments: []
  },
  {
    id: '4',
    name: 'Primer Tarea Bancolombia',
    limitDate: 1575694800000,
    complete: false,
    project : {
      id: '1'
    },
    comments: []
  },
  {
    id: '5',
    name: 'Primer Tarea EPM',
    limitDate: 1575694800000,
    complete: false,
    project : {
      id: '3'
    },
    comments: []
  },
  {
    id: '6',
    name: 'Segunda Tarea Bancolombia',
    limitDate: 1575694800000,
    complete: false,
    project : {
      id: '1'
    },
    comments: []
  }
];
