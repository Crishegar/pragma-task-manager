export interface IUser {
  // id: string;
  name: string;
  lastName: string;
  password: string;
  email: string;
  username: string;
  projects: { id: string }[];
  tasks: { id: string }[];
}

export const users: IUser[] = [
  {
    username: 'cristian.garzon',
    password: '1234',
    name: 'Cristian',
    lastName: 'Garzon',
    email: 'cristian.garzon1495@gmail.com',
    projects: [
      {
        id: '1'
      },
      {
        id: '2'
      }
    ],
    tasks: [
      {
        id: '1'
      },
      {
        id: '2'
      },
      {
        id: '6'
      }
    ]
  },
  {
    username: 'Admin',
    password: 'admin',
    name: 'Administrador',
    lastName: 'Plataforma',
    email: 'cristian.garzon@pragma.com.co',
    projects: [
      {
        id: '1'
      },
      {
        id: '2'
      },
      {
        id: '3'
      },
    ],
    tasks: [
      {
        id: '1'
      },
      {
        id: '2'
      },
      {
        id: '3'
      },
      {
        id: '4'
      },
      {
        id: '5'
      },
      {
        id: '6'
      }
    ]
  }
];
