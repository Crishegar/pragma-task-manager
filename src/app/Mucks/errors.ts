export const errors = {
  required: 'This field is required',
  email: 'Required a valid email',
  invalidPass: 'The pass do not equals',
  invalidUser: 'The user or pass is invalid',
  usernameExist: 'The username already taken',
  invalidDate: 'The format is invalid',
  existName: 'The name is already taken'
};
