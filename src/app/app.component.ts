import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from './home/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  title = 'My Task Manager';
  username = 'Username';

  constructor(public auth: AuthService, private router: Router) {
    const user = localStorage.getItem('task-user');
    if (!!user) auth.existLogUser(user);
    this.router.navigate(['home']);
  }

  logout() {
    this.auth.logout();
  }
}
