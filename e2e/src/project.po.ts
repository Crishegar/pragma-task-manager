import {browser, by, element} from 'protractor';

export class ProjectPage {
  navigateTo() {
    return browser.get('/');
  }

  getProjectButton() {
    return element(by.css(`[routerlink="/projects"]`));
  }

  getProjectComponent() {
    return element(by.css('app-projects'));
  }

  getProjectsCard() {
    const projectsCard = this.getProjectComponent().all(by.css('app-card'));
    return !!projectsCard ? projectsCard : null;
  }

  getNumberOfProjectsCard() {
    const projectCards = this.getProjectsCard();
    return !!projectCards ? projectCards.count() : null;
  }
}
