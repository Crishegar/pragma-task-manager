import {ProfilePage} from './profile.po';
import {browser, by, element} from 'protractor';

describe('Profile page',() => {
  let profilePage;

  beforeEach(() => {
    profilePage = new ProfilePage();
    browser.executeScript(`window.localStorage.setItem('task-user', 'Admin');`);
    profilePage.navigateTo();
    element(by.css('#profile')).click();
  });

  afterEach(() => {
    browser.executeScriptWithDescription(`window.localStorage.removeItem('task-user');`, "set LocalStorage");
  });

  it('Should exist Profile Component', () => {
    expect(profilePage.getProfileComponent()).toBeTruthy();
  });

  it('Should exist User Form Component', () => {
    expect(profilePage.getUserFormComponent()).toBeTruthy();
  });

  it('Should user form is full', () => {
    const inputs = profilePage.getUserFormComponent().all(by.css('input'));
    expect(profilePage.getUserFormComponent()).toBeTruthy();
    expect(inputs.count()).toEqual(4);
  });

  it('Should be display new inputs with click changePassword', () => {
    const changePasswordSpan= profilePage.getUserFormComponent().element(by.css('div.helper-span'));
    const changePasswordSpanText = changePasswordSpan.element(by.css('strong')).getText();
    expect(changePasswordSpanText).toEqual('+ ChangePassword');
    changePasswordSpan.click();
    browser.pause();
    const inputs = profilePage.getUserFormComponent().all(by.css('input'));
    expect(inputs.count()).toEqual(6);
  });

  it('Should the save button are disabled', () => {
    expect(profilePage.getSaveButton().getAttribute('disabled')).toBeTruthy();
  });

  it('Should the save button are enabled', () => {
    const inputs = profilePage.getUserFormComponent().all(by.css('input'));
    const username = inputs.get(0);
    username.sendKeys('Admin');
    expect(profilePage.getSaveButton().getAttribute('disabled')).toBeFalsy();
  });

  it('Should the save button are enabled with change password', () => {
    const changePasswordSpan= profilePage.getUserFormComponent().element(by.css('div.helper-span'));
    changePasswordSpan.click();
    const inputs = profilePage.getUserFormComponent().all(by.css('input'));
    const password = inputs.get(4);
    const confirmationPassword = inputs.get(5);
    password.sendKeys('1234');
    expect(profilePage.getSaveButton().getAttribute('disabled')).toBeTruthy();
    confirmationPassword.sendKeys('1234');
    expect(profilePage.getSaveButton().getAttribute('disabled')).toBeFalsy();
  });
});
