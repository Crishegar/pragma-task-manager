import {browser, by, element} from 'protractor';

export class ProfilePage {
  navigateTo() {
     return browser.get('/');
  }

  getProfileComponent() {
    return element(by.css('app-profile'));
  }

  getUserFormComponent(){
    return element(by.css('app-user-form form'));
  }

  getSaveButton(){
    return element(by.css('app-profile app-card mat-card mat-card-actions div button'));
  }
}
