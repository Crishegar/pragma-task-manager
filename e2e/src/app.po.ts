import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/home');
  }

  getNav() {
    return element(by.css('app-root nav'));
  }

  getHomeContent() {
    return element(by.css('section app-home'));
  }

  getPrjoectsButton() {
    return element(by.css(`[routerlink="/projects"]`));
  }

  getProjectButtonText() {
    return this.getPrjoectsButton().element(by.css('span')).getText();
  }

  getProfileButton() {
    return element(by.css(`#profile`));
  }

  getProfileButtonText() {
    return this.getProfileButton().element(by.css('span')).getText();
  }

  getLogoutButton() {
    return element(by.css(`#logout`));
  }

  getLogoutButtonText() {
    return this.getLogoutButton().element(by.css('span')).getText();
  }

  getRegisterButton() {
    return this.getHomeContent().element(by.css('#registerButton'));
  }

  getRegisterButtonText() {
    return this.getRegisterButton().element(by.css('span')).getText();
  }
}
