import {LoginPage} from './login.po';
import {browser, by, Key} from 'protractor';

describe('Login Page',  () => {
  let loginFormPage;
  beforeEach(() => {
    loginFormPage = new LoginPage();
    loginFormPage.navigateTo();
  });

  it('Should exist loginForm', () => {
    expect(loginFormPage.getLoginFormCard()).toBeTruthy();
  });

  it('Should have a Login title', () => {
    expect(loginFormPage.getLoginFormTitle()).toEqual('Login');
  });

  it('Should have a login form', () => {
    expect(loginFormPage.getLoginForm()).toBeTruthy();
  });

  it('Should have a login form button', () => {
    expect(loginFormPage.getLoginFormButton()).toBeTruthy();
    expect(loginFormPage.getLoginFormButtonText()).toEqual('Login');
  });

  it('Should the login form button is disabled', () => {
    expect(loginFormPage.getLoginFormButton().getAttribute('disabled')).toBeTruthy();
  });

  it(`Should the login form has two inputs`, () => {
    const inputs = loginFormPage.getLoginForm().all(by.css('input'));
    expect(inputs.count()).toEqual(2);
    const usernameInput = inputs.get(0);
    const passwordInput = inputs.get(1);
    expect(usernameInput.getAttribute('placeholder')).toEqual('Username');
    expect(passwordInput.getAttribute('placeholder')).toEqual('password');
  });
});
