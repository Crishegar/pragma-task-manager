import {AppPage} from './app.po';
import {browser, by, element, Key} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  beforeEach(() => {
    page = new AppPage();
  });

  describe('without login user', () => {

    beforeEach(() => {
      page.navigateTo();
    });

    it('should nav have 1 item', () => {
      const arrayTabs = page.getNav().all(by.css('a'));
      expect(arrayTabs.count()).toEqual(1);
    });

    it('should not have task and have login form', ()=> {
      const loginForm = page.getHomeContent().element(by.css('app-loginform'));
      expect(loginForm).toBeTruthy();
    });

    it('should have a register button', () => {
      expect(page.getRegisterButton()).toBeTruthy();
      expect(page.getRegisterButtonText()).toEqual('Register');
    });

    it('should have a display register Form', () => {
      page.getRegisterButton().click();
      browser.pause();
      const registerFormCard = element(by.css('app-signup-form app-card mat-card'));
      const registerTitle = registerFormCard.element(by.css('mat-card-title')).getText();
      expect(registerFormCard).toBeTruthy();
      expect(registerTitle).toEqual('Register');
      browser.actions().sendKeys(Key.ESCAPE).perform();
      !expect(registerTitle).toBeTruthy();
    });
  });

  describe('with Login User', () => {
    beforeEach(() => {
      browser.executeScriptWithDescription(`window.localStorage.setItem('task-user', 'Admin');`, "set LocalStorage");
      page.navigateTo();
    });

    afterEach(() => {
      browser.executeScriptWithDescription(`window.localStorage.removeItem('task-user');`, "set LocalStorage");
    });

    it('should nav have 4 elements', ()=> {
      const arrayTabs = page.getNav().all(by.css('a'));
      expect(arrayTabs.count()).toEqual(4);
    });

    it('should have task and have not login form', ()=> {
      const tasksTitle = page.getHomeContent().element(by.css('div h1'));
      const addTaskButton = page.getHomeContent().element(by.css('div button'));
      expect(tasksTitle.getText()).toEqual('Tasks');
      expect(addTaskButton).toBeTruthy();
    });

    it('should exist Projects button', () => {
      expect(page.getPrjoectsButton()).toBeTruthy();
      expect(page.getProjectButtonText()).toEqual('Projects');
    });

    it('should exist Profile button', () => {
      expect(page.getProfileButton()).toBeTruthy();
      expect(page.getProfileButtonText()).toEqual('Profile');
    });

    it('should exist Logout button', () => {
      expect(page.getLogoutButton()).toBeTruthy();
      expect(page.getLogoutButtonText()).toEqual('Logout');
    });
  });
});
