import {browser, by} from 'protractor';
import {ProjectPage} from './project.po';

describe('Project Page', () =>  {
  let projectPage;
  beforeEach(() => {
    projectPage = new ProjectPage();
    browser.executeScript(`window.localStorage.setItem('task-user', 'Admin');`);
    projectPage.navigateTo();
    projectPage.getProjectButton().click();
  });

  it('Should exist project component', () => {
    expect(projectPage.getProjectComponent()).toBeTruthy();
  });

  it('Should open details card', () => {
    const projects = projectPage.getNumberOfProjectsCard();
    if(!!projects) {
      const project = projectPage.getProjectsCard().get(0);
      const tasks = project.element(by.css('mat-card-content mat-list')).all(by.css('mat-list-item'));
      const firstTaskButton = tasks.get(0).element(by.css('button'));
      firstTaskButton.click();
      browser.pause();
      const taskUrl = browser.driver.getCurrentUrl();
      expect(taskUrl).toContain('projects/task');
    } else {
      expect(projects).toBeNull();
    }
  });

});
