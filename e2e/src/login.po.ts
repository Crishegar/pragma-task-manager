import {browser, by, element} from 'protractor';

export class LoginPage {

  navigateTo(){
    return browser.get('/home');
  }

  getLoginFormCard(){
    return element(by.css('app-loginform app-card mat-card'));
  }

  getLoginFormTitle() {
    return this.getLoginFormCard().element(by.css('mat-card-title')).getText();
  }

  getLoginForm() {
    return this.getLoginFormCard().element(by.css('mat-card-content div form'));
  }

  getLoginFormButton() {
    return this.getLoginForm().element(by.css('button'));
  }

  getLoginFormButtonText() {
    return this.getLoginFormButton().getText();
  }
}
